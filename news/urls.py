from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^add/$', views.SignUpCreate.as_view(), name = "signup-add"), # "SignUpCreate" has been registered with "as_view()" because Urls require a function to be passed not a class.
	url(r'^(?P<pk>[0-9]+)/delete/$', views.SignUpDelete.as_view(), name = "signup-delete"),
	url(r'^(?P<pk>[0-9]+)/update/$', views.SignUpUpdate.as_view(), name = "signup-update"),
	url(r'^$', views.SignUpList.as_view(), name = "signup-list"),
]