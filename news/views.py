from django.shortcuts import render
from django.views import generic
from .models import SignUp
from .forms import SignUpForm
from django.core.urlresolvers import reverse_lazy

# Create your views here.

def home(request):
	return render(request, "home.html", { 'title': "Hello: {0}".format(request.user) }) # Passing the variable 'title'.

class SignUpCreate(generic.edit.CreateView): # Created SignUpCreate class.
	model = SignUp
	form_class = SignUpForm
	success_url = reverse_lazy('signup-list')

class SignUpUpdate(generic.UpdateView):
	model = SignUp
	form_class = SignUpForm
	success_url = reverse_lazy('signup-list')

class SignUpDelete(generic.DeleteView):
	model = SignUp
	success_url = reverse_lazy('signup-list')

# Sign up list and detail
class SignUpList(generic.ListView):
	model = SignUp

class HomePage(SignUpCreate):
	template_name = "news/home.html"

	def get_context_data(self, **kwargs):
		context = super(HomePage, self).get_context_data(**kwargs)
		context['title'] = "Hello: {0}".format(self.request.user)

		return context