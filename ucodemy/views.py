from news.views import HomePage as NewsHomePage
from cslist.views import cslist_context_data

class HomePage(NewsHomePage):
	 template = "home.html"

	 def get_context_data(self, **kwargs):
	 	context = super(HomePage, self).get_context_data(**kwargs)
	 	cslist_context_data(context, self.request)
	 	return context

from django.core.mail import send_mail
from django.http import HttpResponse

def email(request):
	result = "Succesfully sent." if send_mail("Subject", "Message", 'sherif.acmilan@gmail.com', 
		[ 'sm36165@ubt-uni.net' ]) == 1 else "Failed to sent email."
	return HttpResponse(result)